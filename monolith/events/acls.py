from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_picture(search):
    url = "https://api.pexels.com/v1/search?query=" + search
    headers = {"Authorization": PEXELS_API_KEY}

    r = requests.get(url, headers=headers)
    r = json.loads(r.content)
    photos = r["photos"]
    return photos


def get_weather(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (TypeError, KeyError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        weather = {
            "temperature": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
        return weather
    except (KeyError, TypeError):
        return None
